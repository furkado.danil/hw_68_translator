@extends('layouts.app')

@section('content')


    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('translator.store')}}" method="post">
                <div class="modal-body">
                    @csrf
                    <div class="row">
                        <div class="col">
                            <input type="text" class="form-control" name="paragraph" placeholder="@lang('sentence')">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">@lang('Add')</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row mt-4">
        <h3>@lang('Translate')</h3>
    @if(Auth::check())
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary ml-3" data-toggle="modal" data-target="#exampleModal">
            @lang('Add sentence')
        </button>
    @endif
    </div>
    <div class="row mt-3">
        @if($locale)
        @foreach($paragraphs as $paragraph)
            <a href="{{route('translator.show', ['translator' => $paragraph])}}" class="card-link"><p class="text-truncate">{{ $paragraph->paragraph }}</p></a>
        @endforeach
        @endif
    </div>

@endsection