@extends('layouts.app')

@section('content')
    <div class="row mt-4">
        <a class="btn btn-primary" href="{{route('translator.index')}}" role="button">@lang('Back')</a>
    </div>
    <div class="mt-4">
        <h3>{{$translate->paragraph}}</h3>
        @if('')
        <h4>{{$translate->translate('en')->paragraph}}</h4>
        @else
            {{$translate->translate('ru')->paragraph}}
        @endif
        <hr class="my-4">
        <form action="{{route('translator.update', ['translator' => $translate])}}" method="post">
            @csrf
            @method('PUT')
            <h5>@lang('Translate to English')</h5>
            <input type="text" name="paragraph:en" />

            <h5>@lang('Translate to Russian')</h5>
            <input type="text" name="paragraph:ru" />

            <h5>@lang('Translate into French')</h5>
            <input type="text" name="paragraph:fr" />

            <h5>@lang('Translate into Italian')</h5>
            <input type="text" name="paragraph:it" />

            <h5>@lang('Translate into German')</h5>
            <input type="text" name="paragraph:de" />
            <button type="submit" class="btn btn-primary">@lang('Add')</button>
        </form>

    </div>
@endsection