<?php

use Illuminate\Database\Seeder;

class InterpreterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Interpreter::class, 10)->create();
    }
}
