<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
class CreateInterpreterTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interpreter_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('interpreter_id')->constrained()->cascadeOnDelete();
            $table->string('locale')->index();
            $table->string('paragraph');
            $table->unique(['interpreter_id', 'locale']);
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interpreter_translations');
    }
}
