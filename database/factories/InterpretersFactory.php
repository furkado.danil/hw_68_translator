<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Interpreter;
use Faker\Generator as Faker;

$factory->define(Interpreter::class, function (Faker $faker) {
    return [
        'paragraph' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'user_id' => rand(1, 5)
    ];
});
