<?php

namespace App\Policies;

use App\Interpreter;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class InterpreterPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Interpreter  $interpreter
     * @return mixed
     */
    public function view(User $user, Interpreter $interpreter)
    {
        return $user->id == $interpreter->user_id;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Interpreter  $interpreter
     * @return mixed
     */
    public function update(User $user, Interpreter $interpreter)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Interpreter  $interpreter
     * @return mixed
     */
    public function delete(User $user, Interpreter $interpreter)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\Interpreter  $interpreter
     * @return mixed
     */
    public function restore(User $user, Interpreter $interpreter)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Interpreter  $interpreter
     * @return mixed
     */
    public function forceDelete(User $user, Interpreter $interpreter)
    {
        //
    }
}
