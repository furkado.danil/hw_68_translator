<?php

namespace App\Http\Controllers;

use App\Http\Requests\InterpreterRequest;
use App\Interpreter;
use Illuminate\Http\Request;

class InterpreterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $locale = $request->session()->get('locale');
        $paragraphs = Interpreter::all();

        return view('paragraph.index', compact('paragraphs', 'locale'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param InterpreterRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(InterpreterRequest $request)
    {
        $paragraph = new Interpreter();

        $locale = $request->session()->get('locale');

        $paragraph->translateOrNew($locale)->paragraph = $request->get('paragraph');
        $paragraph->user_id = $request->user()->id;
        $paragraph->save();
        return redirect(route('translator.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $translate = Interpreter::findOrFail($id);
        return view('paragraph.show', compact('translate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * @param Request $request
     * @param Interpreter $translate
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Request $request, $id)
    {
        $locale = $request->session()->get('locale');
        $translate = Interpreter::findOrFail($id);
        $translate->update($request->all());
        return view('paragraph.show', compact('translate', 'locale'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
